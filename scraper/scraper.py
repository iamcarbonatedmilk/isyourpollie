import requests
import re
from BeautifulSoup import BeautifulSoup,SoupStrainer
import sys


def getxmllinks():
    links = SoupStrainer("a")
    page = requests.get('http://www.aph.gov.au/Parliamentary_Business/Hansard/Hansreps_2011')
    soup = BeautifulSoup(page.text,parseOnlyThese=links)
    linklist = soup.findAll(href=re.compile("hansardr"))
    xmlfiles = list()
    for link in linklist:
        newpage=requests.get(link['href'])
        soup = BeautifulSoup(newpage.text)
        downloadlinklist = soup.findAll('a')
        for downloadlink in downloadlinklist:
            if downloadlink.text == "View/Save XML":
                xmllink = "http://parlinfo.aph.gov.au/" + str(downloadlink['href'])
                print xmllink
                xmlfiles.append(xmllink)
    return (xmlfiles)




def getlinklist():
    f = open('newurllist.txt', 'r')
    linklist=list()
    for line in f:
        filename = re.split('toc_unixml/|;',line.strip())[1]
        xmlfile=requests.get(line.strip())
        with open(filename,'w') as outfile:
            outfile.write(xmlfile.content)
            outfile.close()

    return()

getlinklist()
downloadxml()

